import React from 'react';
import SideNav from './components/SideNav';
import Main from './components/Main';
import Header from './components/Header';

const App = () => (
  <div>
    <SideNav />
  	<Header />
    <Main />
  </div>
)

export default App;
