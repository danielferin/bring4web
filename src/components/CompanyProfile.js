import React from 'react';
import { Jumbotron, Button } from 'reactstrap';
import '../App.css';
import Iframe from 'react-iframe';

const CompanyProfile = (props) => (
   <div>
    <center><h2> Company Profile </h2></center>

    <br/>
    <center><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Spotify_logo_with_text.svg/500px-Spotify_logo_with_text.svg.png" alt="Smiley face" width="250" height="75"></img></center>
    <br/>
    <center><h4>Member since: 4th September 2016 </h4></center>
    <br/>
    <center><Iframe align="middle" url="http://www.youtube.com/embed/8S37br5cYP8"
            position="center"
            width="30%"
            height="30%"
            styles={{height: "300px"}}
            allowFullScreen/></center>
    <br/>
    <center><h4>Views: 403 </h4></center>
    <center><h4>Job posts: 5 </h4></center>
    <center><h4>Applicants: 23 </h4></center>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
   </div>
)

export default CompanyProfile