import React, { Component } from 'react';
import * as firebase from 'firebase';
import { Button, Grid, Row, Col } from 'react-bootstrap';
import '../App.css';
import attachIcon from '../attachIcon.png';


export class WebCamera extends Component {


  constructor(props, context){
    super(props, context);

    this.state = {
      showTimer: false,
      cameraIsEnabled: false,
      isRecording: false,
      hasData: false,
      recordedChunks: [],
      mediaRecorder: null,
      src: null,
      stream: null,
      blob: null,
      time: {},
      seconds: 0,
      jobPostKey: null,
      recordHeaderText: "Record job post presentation",
      videoNr: 0,
      showPlayer: true,
      showONBtn: true,
      showRecBtn: false,
      showDoneBtn: true,
      showCheckList: false,
      checkListItems: [],
      questions: [],
      recBtnLabel: "rec"

    };

    this.timer = 0;
    this.startTimer = this.startTimer.bind(this);
    this.countUp = this.countUp.bind(this);
    this.secondsToTime = this.secondsToTime.bind(this);
    this.resetValues = this.resetValues.bind(this);
    this.enableCamera = this.enableCamera.bind(this);
    this.handleRecording = this.handleRecording.bind(this);
    this.stopRecording = this.stopRecording.bind(this);
    this.playHandler = this.playHandler.bind(this);
    this.downloadHandler = this.downloadHandler.bind(this);
    this.storeOnFireBase = this.storeOnFireBase.bind(this);
    this.guid = this.guid.bind(this);
    this.s4 = this.s4.bind(this);
    this.goToJobPosts = this.goToJobPosts.bind(this);
    this.createCheckList = this.createCheckList.bind(this);
    this.updateCheckList = this.updateCheckList.bind(this);
    this.promptBoxHandler = this.promptBoxHandler.bind(this);
    this.onItemClick = this.onItemClick.bind(this);

  }


  componentDidMount() {
    this.createCheckList();
    this.setState({showDoneBtn: false});
  }


  componentWillUnmount(){
    clearInterval(this.timer);
  }


  secondsToTime(secs){
    let hours = Math.floor(secs / (60 * 60));
    let divisor_for_minutes = secs % (60 * 60);
    let minutes = Math.floor(divisor_for_minutes / 60);
    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);
    let obj = {
      "h": hours,
      "m": minutes,
      "s": seconds
    };
    return obj;
  }


  startTimer() {
    this.timer = setInterval(this.countUp, 1000);
  }


  countUp() {
    let seconds = this.state.seconds + 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds,
    });
  }


  enableCamera() {
    navigator.mediaDevices.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;
    if (navigator.getUserMedia) {
      this.setState({cameraIsEnabled: true});
      navigator.getUserMedia({ audio: true, video: true },
        function(stream) {
          var newMediaRecorder = new MediaRecorder(stream, {type : 'video/webm;codecs=vp9'});
          this.setState({mediaRecorder: newMediaRecorder});
          this.setState({mediaRecorder: document.querySelector("#streamedVideo").volume = 0});
          this.setState({src: window.URL.createObjectURL(stream)});
          this.setState({stream: stream});
        }.bind(this),
        function(err) {
           console.log("The following error occurred: " + err.name);
        }
      );
    } else {
      console.log("getUserMedia not supported by this browser");
    }
  };


  handleRecording(){
    const stream = this.state.stream;
    const recordedChunks = [];
    const mediaRecorder = new MediaRecorder(stream, {type : 'video/webm;codecs=vp9'});
    const _this = this;
    if(this.state.blob){
      const stream = this.state.stream;
      const video = document.querySelector("#streamedVideo");
      video.volume = 0;
      const videoURL = window.URL.createObjectURL(stream);
      video.src = videoURL;
    }
    this.resetValues();
    this.startTimer();
    mediaRecorder.start();
    mediaRecorder.ondataavailable = function(e) {
      if (e.data.size > 0){
        recordedChunks.push(e.data);
        _this.setState({recordedChunks:recordedChunks});
      }
    }
    mediaRecorder.onstop = function(e) {
      if (recordedChunks){
        if(recordedChunks.length > 0) {
          const video = document.querySelector("#streamedVideo");
          video.controls = true;
          const blob = new Blob(recordedChunks, { 'type' : 'video/webm;codecs=vp9'});
          const videoURL = window.URL.createObjectURL(blob);
          _this.setState({blob:blob});
          video.src = videoURL;
        }
      }
    }
     this.setState({mediaRecorder:mediaRecorder});
  }


  resetValues(){
    clearInterval(this.timer);
    this.setState({
      seconds: 0,
      time: [],
      showTimer: true,
      isRecording: true,
      hasData: false
    });
  }


  stopRecording(){
    const isRecording = this.state.isRecording;
    const mediaRecorder = this.state.mediaRecorder;
    if(isRecording){
      mediaRecorder.stop();
      clearInterval(this.timer);
      this.setState({isRecording: false,
        mediaRecorder:mediaRecorder,
        hasData: true,
        recBtnLabel: "re-record"
      });
      document.getElementById("streamedVideo").volume = 1;
    }
  }


  playHandler(){
    const blob = this.state.blob;
    if(blob != null){
      const video = document.querySelector("#streamedVideo");
      const blob = this.state.blob;
      const videoURL = window.URL.createObjectURL(blob);
      video.src = videoURL;
    }
  }


  downloadHandler(){
    const recordedBlob = this.state.blob;
    if(recordedBlob != null){
      var url = window.URL.createObjectURL(recordedBlob);
      var a = document.createElement('a');
      var filename = prompt("Please write filename to the video", "video_1");

      if (filename != null) {
        a.download = filename + '.webm';
        a.style.display = 'none';
        a.href = url;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
          document.body.removeChild(a);
          window.URL.revokeObjectURL(url);
        }, 100);
      }
    }
  }

  //skapar unikt id
  guid() {
    return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' +
      this.s4() + '-' + this.s4() + this.s4() + this.s4();
  }

  s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }


  storeOnFireBase(){
    firebase.auth().onAuthStateChanged(firebaseUser => {

      if(firebaseUser) {

        if(!this.state.blob){
          alert("no recorded video found!");
        }else{

          var storageDownloadURL = '';
          var uniqueKey = this.props.jobPostKey;
          var videoNr = this.state.videoNr;
          const uuid = this.guid();
          const blobFile = this.state.blob;
          blobFile.name = uuid + ".webm";
          const metadata = { contentType: 'video/webm;codecs=vp9'};
          const storageRef = firebase.storage().ref();
          const uploadTask = storageRef.child('intervjuer_videos/' + blobFile.name).put(blobFile, metadata);
          const rootRef = firebase.database().ref('jobPosts');

          uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
            function(snapshot) {
              var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              console.log('Upload is ' + progress + '% done');
              switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED:
                  console.log('Upload is paused');
                  break;
                case firebase.storage.TaskState.RUNNING:
                  //console.log('Upload is running');
                  break;
                default:
                  break;
              }
            }, function(error) {
              switch (error.code) {
                case 'storage/unauthorized':
                    alert("User doesn't have permission to access the object");
                  break;
                case 'storage/canceled':
                    console.log("User canceled the upload");
                  break;
                case 'storage/unknown':
                    console.log("Unknown error occurred, inspect error.serverResponse");
                  break;
                default:
                  break;
              }
          }, function() {
            storageDownloadURL = uploadTask.snapshot.downloadURL;
            rootRef.child(uniqueKey).child("videoNr_"+videoNr).set({
              uniqueJobid: uniqueKey,
              downloadURL: storageDownloadURL
            });
          });
        }

        //this.setState({videoNr: (this.state.videoNr + 1)});
        this.setState({
          recordHeaderText: ("Record Question " + this.state.videoNr),
          hasData: false,
          showTimer: false,
          recBtnLabel: "rec"
        });
        const stream = this.state.stream;
        const video = document.querySelector("#streamedVideo");
        const videoURL = window.URL.createObjectURL(stream);
        video.src = videoURL;
        this.updateCheckList();
        //setTimeout(() => {
          //this.promptBoxHandler();
        //}, 500);

      }else{
        alert("You are not logged in.");
      }

    });

  }


  goToJobPosts(){
    window.location = './Applicants';
  }


  createCheckList(){
    const arr = [{
        "key": "  Record presentation of the job",
        "value": false,
        "border": "1px dashed black",
        "color": "#16b7cd",
        "curser": "pointer"
      },
      {
        "key": "  Record question 1   ",
        "value": false,
        "border": "1px dashed black",
        "color": "#16b7cd",
        "curser": "pointer"
      },
      {
        "key": "  Record question 2   ",
        "value": false,
        "border": "1px dashed black",
        "color": "#16b7cd",
        "curser": "pointer"
      },
      {
        "key": "  Record question 3   ",
        "value": false,
        "border": "1px dashed black",
        "color": "#16b7cd",
        "curser": "pointer"
      }
    ];

    this.setState({
      checkListItems: arr,
      showCheckList: true
    });
  }


  updateCheckList(){
    var index = this.state.videoNr;
    var truthCounter = 0;
    var arr = this.state.checkListItems;
    arr[index].value = true;
    this.setState({checkListItems: arr});

    for (var i = 0; i < this.state.checkListItems.length; i++) {
      if(this.state.checkListItems[i].value === true){
        truthCounter += 1;
        arr[index].border = "1px dashed green";
        arr[index].color = "lightgrey";
        arr[index].curser = "not-allowed";
      }
    }

    this.setState({
      checkListItems: arr,
      cameraIsEnabled: false,
      showPlayer: false
    });

    this.props.updateProgressBar();

    if(truthCounter === this.state.checkListItems.length){
      this.setState({showDoneBtn: true,
        recordHeaderText: (""),
        cameraIsEnabled: false,
        showPlayer: false,
      });
    }

  }


  promptBoxHandler(){
    var arr = this.state.questions;
    var index = this.state.videoNr;
    if(index < 4){
      var q = prompt("Please write question " + index + ".","");
      if (q === null || q === "") {
        console.log("User cancelled the prompt.")
      } else {
        arr.push(q);
        const rootRef = firebase.database().ref('jobPosts');
        const uniqueKey = this.props.jobPostKey;
        rootRef.child(uniqueKey).child("questionNr_"+index).set({questionValue: q});
      }
    }
    if(index > 3){
      this.setState({showPlayer: false});
    }
  }


  onItemClick(value){
    if(this.state.checkListItems[value].value === false){
      this.setState({videoNr: value});
      var arr = this.state.checkListItems;
      for (var i = 0; i < arr.length; i++) {
        if(value === i){
          arr[i].border = "2px dashed #16b7cd";
        }else{
          if(arr[i].value === true){
            arr[i].border = "1px dashed green";
          }else{
            arr[i].border = "1px dashed black";
          }
        }
      }
      this.setState({checkListItems: arr});
      if(!this.cameraIsEnabled){
        this.enableCamera()
        this.setState({showPlayer: true});
      }
    }
  }




  render(){

    const divStyle = {
      color: '#16b7cd',
      background: 'rgba(22, 183, 205, 0.15)'
    };


    return(
      <div>
        <h2 style={{color:'#16b7cd', textAlign: "center"}}>Step 3: Record Interview Questions</h2>
        <Grid>
          <Row className="show-grid">
            <Col xs={12} md={8}>
              <div className="webCamera_rightAlign">
                <br/><br/>
                <div id="videoContainer">
                  {this.state.showPlayer &&
                    <div>
                      <video id="streamedVideo" src={this.state.src} autoPlay="true"></video>
                    </div>
                  }
                </div>

                {/*
                {this.state.showONBtn &&
                  <div>
                    <br/>
                    <Button style={divStyle} type="submit" onClick={this.enableCamera.bind(this)} >
                      ON
                    </Button>
                  <br/><br/>
                  </div>
                }*/}

                {this.state.showTimer &&
                  <h4 style={{color: "red"}}> {this.state.time.m}m {this.state.time.s}s</h4>
                }

                {this.state.cameraIsEnabled && !this.state.showDoneBtn &&
                  <div>
                    <br/>
                    {!this.state.isRecording &&
                      <div>
                        {/*<h4 style={{color: "#16b7cd"}}> {this.state.recordHeaderText}</h4>
                        <br/>*/}
                        <Button style={divStyle} type="submit" onClick={()=>this.handleRecording()} >
                          {this.state.recBtnLabel} 🔴
                        </Button>
                      </div>
                    }

                    {this.state.isRecording &&
                      <Button style={divStyle} type="submit" onClick={()=>this.stopRecording()} >
                        stop ⏹️
                      </Button>
                    }

                    {this.state.hasData &&
                      <div>
                        <br/>
                        {/*
                        <Button style={divStyle} onClick={()=>this.playHandler()} >
                        play ▶️
                        </Button>
                        &nbsp;&nbsp;
                        <Button style={divStyle} onClick={()=>this.downloadHandler()} >
                        download 💾
                        </Button>
                        &nbsp;&nbsp;*/}
                        <Button style={divStyle} onClick={()=>this.storeOnFireBase()} >
                          save
                        </Button>
                      </div>

                    }

                  </div>
                }

                {this.state.showDoneBtn &&
                  <div>
                  <br/><br/><br/>
                  <Button onClick={this.goToJobPosts}>
                    done
                  </Button>
                  </div>
                }

              </div>
            </Col>

            <Col xs={6} md={4}>
              <br/><br/><br/>
              <div className="webCamera_leftAlign">
                {this.state.showCheckList &&
                  <div className="checkList" >

                    <div className="checkListItem" style={{cursor: this.state.checkListItems[0].curser, border: this.state.checkListItems[0].border, color: this.state.checkListItems[0].color}} onClick={() => this.onItemClick(0)}>
                      <input type="checkbox" disabled checked={this.state.checkListItems[0].value}  value={this.state.checkListItems[0].key} />
                        {this.state.checkListItems[0].key}
                        &nbsp; <img className="attachIcon-profile" src={attachIcon} alt="attachIcon"/>
                        <br/>
                    </div><br/>

                    <div className="checkListItem" style={{cursor: this.state.checkListItems[1].curser, border: this.state.checkListItems[1].border, color: this.state.checkListItems[1].color}} onClick={() => this.onItemClick(1)}>
                      <input type="checkbox" disabled checked={this.state.checkListItems[1].value}  value={this.state.checkListItems[1].key} />
                        {this.state.checkListItems[1].key}
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <img className="attachIcon-profile" src={attachIcon} alt="attachIcon"/>
                        <br/>
                      </div><br/>

                    <div className="checkListItem" style={{cursor: this.state.checkListItems[2].curser, border: this.state.checkListItems[2].border, color: this.state.checkListItems[2].color}} onClick={() => this.onItemClick(2)}>
                      <input type="checkbox" disabled checked={this.state.checkListItems[2].value}  value={this.state.checkListItems[2].key} />
                        {this.state.checkListItems[2].key}
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <img className="attachIcon-profile" src={attachIcon} alt="attachIcon"/>
                        <br/>
                      </div><br/>

                    <div className="checkListItem" style={{cursor: this.state.checkListItems[3].curser, border: this.state.checkListItems[3].border, color: this.state.checkListItems[3].color}} onClick={() => this.onItemClick(3)}>
                      <input type="checkbox" disabled checked={this.state.checkListItems[3].value}  value={this.state.checkListItems[3].key} />
                        {this.state.checkListItems[3].key}
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <img className="attachIcon-profile" src={attachIcon} alt="attachIcon"/>
                        <br/>
                      </div><br/>

                  </div>
                }
              </div>
            </Col>
          </Row>
        </Grid>


        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
      </div>
    )
  }
}


export default WebCamera;
