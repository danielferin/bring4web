import React from 'react'
import '../App.css';
import { FormGroup, FormControl, Button , ButtonGroup, ProgressBar } from 'react-bootstrap';
import WebCamera from './WebCamera';
import firebase from '../firebase.js';


class Test extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
        uniqueJobPostKey: null,
        companyName: '',
        jobPositionTitle: '',
        jobDescription: '',
        jobType: '',
        industry: 'IT',
        functions: 'function 1',
        location: '',
        date1: '',
        date2: '',
        pageNr: 1,
        showPage1: true,
        showPage2: false,
        showPage3: false,
        showPage3NextBtn: false,
        progressBarValue: 0
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.stepHandler = this.stepHandler.bind(this);
    this.writeToDatabase = this.writeToDatabase.bind(this);
    this.updateProgressBar = this.updateProgressBar.bind(this);

  }


  handleChange(e) {
    this.setState({[e.target.name]: e.target.value});
  }


  handleSubmit(event) {
    this.stepHandler();
    event.preventDefault();
  }


  writeToDatabase() {
      const itemsRef = firebase.database().ref('jobPosts');
      const uniqueJobPostKey = itemsRef.push().key;
      this.setState({
          uniqueJobPostKey: uniqueJobPostKey
      });
      itemsRef.child(uniqueJobPostKey).set({
        companyName: this.state.companyName,
        jobPositionTitle: this.state.jobPositionTitle,
        jobDescription: this.state.jobDescription,
        jobType: this.state.jobType,
        industry: this.state.industry,
        functions: this.state.functions,
        location: this.state.location,
        //date1: this.state.date1,
        //date2: this.state.date2
      });
  }


  onRadioBtnClick(value){
    this.setState({
      jobType: value
    });
  }


  componentDidMount() {}


  stepHandler(){

    var page = this.state.pageNr;
    switch(page) {
      case 1:
        this.setState({
          showPage1: false,
          showPage2: true,
          showPage3: false
        });
        break;
      case 2:
        this.writeToDatabase();
        this.setState({
          showPage1: false,
          showPage2: false,
          showPage3: true,
          showPage3NextBtn: true
        });
        break;
      default:
        console.log("Page missing!");
    }

    this.setState({pageNr: (page + 1)});
    if(page === 3){this.setState({pageNr: 1})}

    setTimeout(() => {
      this.updateProgressBar()
    }, 400);
  }

  updateProgressBar(){
    var progress = this.state.progressBarValue;
    progress += 16.7;
    this.setState({progressBarValue: progress});
  }




  render(){

      const jobPostKey = this.state.uniqueJobPostKey;
      const divStyle = {color: '#16b7cd',background: 'rgba(22, 183, 205, 0.15)'};

      return (
        <center>
          <div className='container'>

            <ProgressBar style={{ width: '25%'}}>
              <ProgressBar active bsStyle="success" now={this.state.progressBarValue} key={1} />
            </ProgressBar>

            {this.state.showPage1 &&
              <div>
                <h2 style={{ color: '#16b7cd' }}>Step 1: Job details</h2>
                <br/>

                <section className='add-item'>
                  <form onSubmit={this.handleSubmit}>
                    <br/><br/>
                    <input
                      id="formControlsText"
                      name="companyName"
                      type="text"
                      label="Text"
                      placeholder="Company name"
                      onChange={this.handleChange}
                      value={this.state.companyName}
                    />
                    <br/><br/>
                    <input
                      id="formControlsText"
                      name="jobPositionTitle"
                      type="text"
                      label="Text"
                      placeholder="Title of Position"
                      onChange={this.handleChange}
                      value={this.state.jobPositionTitle}
                    />
                    <br/><br/>
                    <input
                      id="formControlsText"
                      name="jobDescription"
                      type="textarea"
                      label="Text"
                      placeholder="Job description"
                      onChange={this.handleChange}
                      value={this.state.jobDescription}
                    />
                    <br/><br/>
                    <ButtonGroup>
                      <Button style={divStyle} onClick={() => this.onRadioBtnClick('By The Hour')} active={this.state.jobType === 1}>By the hour</Button>
                      <Button style={divStyle} onClick={() => this.onRadioBtnClick('Part Time')} active={this.state.jobType === 2}>Part Time</Button>
                      <Button style={divStyle} onClick={() => this.onRadioBtnClick('Full Time')} active={this.state.jobType === 3}>Full Time</Button>
                    </ButtonGroup>
                    <br/><br/>
                    <Button style={{color:'#16b7cd'}} type="submit">
                      Next step
                    </Button>
                  </form>

                </section>
              </div>
            }

            {this.state.showPage2 &&
              <div>
                <h2 style={{color:'#16b7cd'}}>Step 2: Details</h2>
                <br/>

                <section className='add-item'>
                  <form onSubmit={this.handleSubmit}>
                    <br/>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        componentClass="select"
                        name="industry"
                        type='text'
                        defaultValue='IT'
                        onChange={this.handleChange}
                        className="dropdownTest">
                        <option value="IT">IT</option>
                        <option value="Media">Media</option>
                        <option value="Movies">Movies</option>
                      </FormControl>
                    </FormGroup>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        componentClass="select"
                        name='functions'
                        type='text'
                        defaultValue='function 1'
                        onChange={this.handleChange}
                        className="dropdownTest">
                        <option value="function 1" >function 1</option>
                        <option value="function 2">function 2</option>
                        <option value="function 3">function 3</option>
                        <option value="function 4">function 4</option>
                      </FormControl>
                    </FormGroup>

                    <input
                      type="text"
                      name="location"
                      placeholder="Location"
                      onChange={this.handleChange}
                      value={this.state.location}
                    />

                    {/*
                    <br/><br/>
                    <input
                      type="text"
                      name="date1"
                      placeholder="Date start"
                      onChange={this.handleChange}
                      style={{ width: '150px' }}
                      value={this.state.date1}
                    />
                    <input type="text"
                      name="date2"
                      placeholder="Date finish"
                      onChange={this.handleChange}
                      style={{ width: '150px' }}
                      value={this.state.date2}
                    />*/}

                    <br/><br/>
                    <Button style={{color:'#16b7cd'}} type="submit">
                      Next step
                    </Button>
                    <br/><br/><br/><br/><br/><br/>
                  </form>
                </section>
              </div>
            }

            {this.state.showPage3 &&
              <div>
                <WebCamera jobPostKey={jobPostKey} updateProgressBar={this.updateProgressBar}/>
              </div>
            }
            <br/><br/><br/>
          </div>
        </center>
      )
    }
}







export default Test
