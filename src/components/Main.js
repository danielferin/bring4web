import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import Jobs from './Jobs'
import Calendar from './Calendar'
import Favorites from './Favorites'
import CreateJob from './CreateJobPost'
import Applicants from './Applicants'
import WebCamera from './WebCamera'
import LoginLogout from './LoginSignout'
import CompanyProfile from './CompanyProfile'
import step1 from './step1'
import step2 from './step2'
import step3 from './step3'
import step4 from './step4'
import logo from '../intervjuer_logo-1alpha0.2.png';


const Main = () => (
  <main id="page-wrap">
      <div style={{ backgroundImage: 'url(' + logo + ')'}}>
      <Switch>
        <Route exact path='/' component={Home}/>
        <Route path='/home' component={Home}/>
        <Route path='/createjob' component={CreateJob}/>
		    <Route path='/applicants' component={Applicants}/>
        <Route path='/jobs' component={Jobs}/>
        <Route path='/calendar' component={Calendar}/>
        <Route path='/favorites' component={Favorites}/>
        <Route path='/createjob' component={CreateJob}/>
        <Route path='/webcamera' component={WebCamera}/>
        <Route path='/loginlogout' component={LoginLogout}/>
        <Route path='/step1' component={step1}/>
        <Route path='/step2' component={step2}/>
        <Route path='/step3' component={step3}/>
        <Route path='/step4' component={step4}/>
        <Route path='/CompanyProfile' component={CompanyProfile}/>
      </Switch>
      </div>
  </main>
)

export default Main


 /*style={{opacity: 0.1, backgroundImage: 'url(' + logo + ')'}}*/
