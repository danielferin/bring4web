import React, { Component } from 'react';
import * as firebase from 'firebase';
import '../App.css';

class LoginSignout extends Component {

  constructor(){
    super();
    this.state = {
      loggedIn: false,
      loginBtn: document.getElementById('loginBtn')

    }

    this.handleClick = this.handleClick.bind(this);
  }


  componentDidMount(){

    firebase.auth().onAuthStateChanged(firebaseUser => {

      /*
      if(!this.state.loggedIn){

        const loginBtn = document.getElementById('loginBtn');
        const signupBtn = document.getElementById('signupBtn');
        const logoutBtn = document.getElementById('logoutBtn');
        const txtEmail = document.getElementById('txtEmail');
        const txtPassword = document.getElementById('txtPassword');
      }*/
      this.setState({img: ""});
      //const txtMsg = document.getElementById('txtMsg');

      if(firebaseUser) {

        var uuid = firebaseUser.uid;
        var LeUser = '';
        var LeImg = '';
        var ref = firebase.database().ref("users/" + uuid);

        ref.on('value', snap => {
          LeUser = snap.val().name;
          this.setState({username: LeUser});
          LeImg = snap.val().profileImageUrl;
          this.setState({img: LeImg});
        });

        this.setState({loggedIn: true});

      }else{

        this.setState({loggedIn: false});

        console.log('not logged in');
      }

    });
  }


  handleClick(event){

    //const txtMsg = document.getElementById('txtMsg');

    if(!this.state.loggedIn){
      var loginBtn = document.getElementById('loginBtn');
      var signupBtn = document.getElementById('signupBtn');
      var txtEmail = document.getElementById('txtEmail');
      var txtPassword = document.getElementById('txtPassword');
      var email = txtEmail.value;
      var pass = txtPassword.value;
    }

    var logoutBtn = document.getElementById('logoutBtn');
    var auth = firebase.auth();
    var selectedButton = event.target;

    if(selectedButton === loginBtn){
      const promise = auth.signInWithEmailAndPassword(email,pass);

      promise.catch(e => window.alert(e.message));
      promise.catch(e => console.log(e.message));
    }
    if(selectedButton === signupBtn){

      const promise = auth.createUserWithEmailAndPassword(email,pass).then(function(value) {
                console.log(value);
                }).catch(function(error) {
                    console.log(error);
                });
      promise.catch(e => window.alert(e.message));
      promise.catch(e => console.log(e.message));
    }
    if(selectedButton === logoutBtn){
      firebase.auth().signOut();
    }
  };



  render() {
    return (
    <center>
      <div style={{paddingTop:'10px'}}>
            <br/>

            {this.state.loggedIn &&
              <div>
                <h4 id="txtMsg"> Logged in as: </h4>
                <h3>{this.state.username}</h3>
                <br/>
                <button id="logoutBtn" className="btn btn-sction" onClick={this.handleClick.bind(this)}>
                 log out
                </button>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
              </div>
            }

            {!this.state.loggedIn &&
              <div>
                <h2 id="txtMsg"> Welcome to Intervjuer </h2>
                <br/><br/><br/>
                <input id="txtEmail" type="email" placeholder="Email"/>
                &nbsp;&nbsp;
                <input id="txtPassword" type="password" placeholder="Password"/>
                <br/>
                <br/>
                <button id="loginBtn" className="btn btn-action" onClick={this.handleClick.bind(this)}>
                 login
                </button>
                &nbsp;&nbsp;
                <button id="signupBtn" className="btn btn-secondary" onClick={this.handleClick.bind(this)}>
                 sign up
                </button>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
              </div>
            }


      </div>
    </center>
    )
  }
}

export default LoginSignout;
