import React from 'react'
import '../App.css';
import 'bootstrap/dist/css/bootstrap.css';
//import { Card, Button, CardImg, CardTitle, CardText, CardDeck, CardSubtitle, CardBlock, ButtonGroup } from 'reactstrap';
import firebase, { auth } from '../firebase.js';
import Icon from 'react-icons-kit';
import { bin, plus } from 'react-icons-kit/icomoon';



class Test extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
        cSelected: [],
        formValues: {},
        /* jobPositionTitle*/
        jobPositionTitle: '',
        /* comapany name*/
        companyName: '',
        location: '',
        jobType: '',
        items: [],
        currentItem: '',
        username: '',
        postArray: [],
        user: null
    }

    this.handleChange = this.handleChange.bind(this);
    //this.handleSubmit = this.handleSubmit.bind(this);

    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);
    this.onCheckboxBtnClick = this.onCheckboxBtnClick.bind(this);
  }

  onRadioBtnClick(rSelected) {
    this.setState({ rSelected });
  }

  onCheckboxBtnClick(selected) {
    const index = this.state.cSelected.indexOf(selected);
    if (index < 0) {
      this.state.cSelected.push(selected);
    } else {
      this.state.cSelected.splice(index, 1);
    }
    this.setState({ cSelected: [...this.state.cSelected] });
  }

  handleChange(e) {
    this.setState({
    [e.target.name]: e.target.value
    });
  }

/*
  handleSubmit(e) {
     e.preventDefault();
        const itemsRef = firebase.database().ref('items');
        const item = {
          title: this.state.currentItem,
          user: this.state.username
        }
        itemsRef.push(item);




        this.setState({
          currentItem: '',
          username: ''
        });


      event.preventDefault();
      console.log(event);

  }*/

  componentDidMount() {

    auth.onAuthStateChanged((user) => {
    if (user) {
      this.setState({ user });
      //console.log(user);
      };
    });

    const itemsRef = firebase.database().ref('jobPosts');
    itemsRef.on('value', (snapshot) => {
      let jobPostObject = snapshot.val();
      let newArray = [];
      for (let uniqueJobPostKey in jobPostObject) {
        newArray.push({
          key: uniqueJobPostKey,
          id: uniqueJobPostKey,
          jobPositionTitle: jobPostObject[uniqueJobPostKey].jobPositionTitle,
          companyName: jobPostObject[uniqueJobPostKey].companyName,
          location: jobPostObject[uniqueJobPostKey].location,
          jobType: jobPostObject[uniqueJobPostKey].jobType
        });
      }
      this.setState({
          postArray: newArray
      });
    });
  }

  removeItem(uniqueJobPostKey) {
    var ref = firebase.database().ref(`/jobPosts/${uniqueJobPostKey}`);
    ref.remove();
  }

  expandView(uniqueJobPostKey) {


  }

  render(){
    return (
      <center>
        <div className='container'>
          <h1 style={{ color: '#16b7cd' }}>Job Posts</h1>
          <br/>
          <section className='display-item'>

          <div className="wrapper">
              <ul>
                {this.state.postArray.map((uniqueJobPostKey, i) => {
                  return (
                    <div className="jobPostItem" key={i}>
                      <li key={uniqueJobPostKey.id}>
                        <h3>{uniqueJobPostKey.jobPositionTitle}</h3>
                        <p>brought by: {uniqueJobPostKey.companyName}</p>
                        <p>{uniqueJobPostKey.jobType}</p>
                        <p>{uniqueJobPostKey.location}</p>
                        <button2 onClick={() => this.removeItem(uniqueJobPostKey.id)}><Icon size={16} icon={plus}/></button2>
                        &nbsp;
                        <button2 onClick={() => this.removeItem(uniqueJobPostKey.id)}><Icon size={16} icon={bin}/></button2>
                      </li>
                    </div>
                  )
                })}
              </ul>
          </div>
          
          </section>
        </div>
      </center>
    )
  }
}

export default Test

