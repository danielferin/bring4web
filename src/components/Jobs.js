import React from 'react'
import '../App.css';

const JobsView = () => (
  <div>
      <center>
        <h2> Jobs </h2>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed congue, purus ac porta placerat, odio metus rutrum massa, et commodo libero neque et libero. Cras sagittis accumsan metus. Donec ut tristique risus. Donec a nunc ut lectus hendrerit elementum. Quisque et aliquam ipsum. Etiam eget eros nibh. Vestibulum venenatis fermentum mauris nec aliquam. Proin consequat, velit et vestibulum laoreet, diam libero fermentum nibh, a euismod est massa eget dui. Suspendisse quis sodales felis. Etiam ac turpis venenatis, luctus ligula in, cursus diam. Integer ornare suscipit sapien et suscipit. Aenean ullamcorper ultricies diam, nec convallis lacus luctus in.
        </p>
        <p>
          Vivamus sed pulvinar libero. Etiam lobortis, dolor at interdum posuere, metus ligula scelerisque libero, id aliquam tortor tellus at leo. Sed sapien eros, tincidunt in eros at, tempor hendrerit nisi. Praesent pharetra sapien arcu, cursus laoreet ante posuere eget. Vivamus elementum lobortis ultrices. Praesent in ex vel nunc molestie laoreet. Pellentesque ac gravida risus, ac accumsan dolor. Aliquam nec rutrum dui, a cursus nisi. Quisque at nibh non elit ornare eleifend. Praesent eget euismod ex. Etiam ipsum lacus, efficitur eu purus ut, efficitur blandit ipsum. Aliquam commodo magna mauris, lacinia condimentum nunc tempor elementum.
        </p>
        <p>
          Praesent sed dolor a turpis sodales pellentesque. Pellentesque iaculis turpis at rutrum sagittis. Vivamus sagittis at purus id rutrum. Integer in justo sed nulla porttitor luctus a non metus. Nullam id quam non diam sagittis aliquet et at tellus. Nam dolor elit, molestie tincidunt auctor vel, malesuada at lorem. Praesent iaculis, erat vitae scelerisque sollicitudin, mauris lectus sagittis diam, ut sodales elit nibh eu leo. Proin cursus nunc sapien, et semper turpis feugiat quis. Proin a sapien a lacus tempor auctor.
        </p>
        <p>
          Praesent posuere ultricies sapien id pretium. Nunc sit amet orci id lacus ullamcorper porta sed vitae felis. Aliquam condimentum consectetur volutpat. Praesent bibendum turpis eget ligula dapibus finibus ut sit amet leo. In in lacus varius, congue nisl et, ullamcorper nunc. Pellentesque lacus tellus, maximus sed magna vel, faucibus fermentum tortor. Donec odio massa, commodo ut blandit id, congue in elit. Duis tincidunt eget urna a dignissim. Etiam hendrerit porttitor tellus, sed bibendum dolor dignissim et. Duis ligula sem, efficitur non ex eu, eleifend sollicitudin mauris.
        </p>
        <p>
          Integer tempor nulla at massa ornare semper. Nam dictum metus magna, in porta libero accumsan in. Integer justo urna, convallis mattis lectus sit amet, porta facilisis justo. Maecenas risus ante, sagittis nec augue non, laoreet facilisis lectus. Duis non risus at nunc venenatis pulvinar in eget dolor. Cras ac pharetra libero. Quisque bibendum ligula lacus, in ultricies risus sodales imperdiet. Sed consectetur tincidunt arcu sit amet lobortis. Ut rutrum magna rhoncus nisl ornare tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vivamus semper porttitor orci ac semper.
        </p>
      </center>
  </div>
)

export default JobsView
