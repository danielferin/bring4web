import React from 'react'
import logo from '../intervjuer_logo-1.png';
import '../App.css';

const Header = () => (
  <div>
    <center>
     <img src={logo} id='headerImage' alt={"logo"} width={200} style={{paddingTop:'15px', paddingBottom:'15px'}} mode='fit' />
    </center>
  </div>
)

export default Header
