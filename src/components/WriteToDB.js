import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import * as firebase from 'firebase';
import '../App.css';

/* gå in här: https://react-bootstrap.github.io/components.html */

class WriteToDB extends Component {


  constructor() {
        super();

        this.state = {
          speed: 33
        };

        this.writeToDatabase = this.writeToDatabase.bind(this);
        this.forsokerRenderGrejs = this.forsokerRenderGrejs.bind(this);
  }


  forsokerRenderGrejs(){
    this.setState({speed: 77});
  };

  componentDidMount(){
    const rootRef = firebase.database().ref().child('react').child('speed');
    rootRef.on('value', snap => {
      this.setState({
        speed: snap.val();
      })
    });
  }

  writeToDatabase(){

    const rootRef = firebase.database().ref().child('react').child('test');
    const messageText = "skriva till databasen = funkar!";
    const newPostKey = firebase.database().ref().child("react").child('test').push().key;

    rootRef.child(newPostKey).set({
      name: messageText,
      id: newPostKey
    });

    //window.alert(newPostKey);
  }



  render() {
    return (

      <div>
        <center>
          <Button bsSize="small" className="writeToDBBtn" onClick={this.forsokerRenderGrejs} >write to database</Button>
          <h1>{this.state.speed}</h1>
        </center>
      </div>

    );
  }
}

export default WriteToDB;
