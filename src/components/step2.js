import React from 'react'
import '../App.css';
import { Link } from 'react-router-dom'

class Test extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            formValues: {}
        }
    }

    handleChange(event) {
        event.preventDefault();
        let formValues = this.state.formValues;
        let name = event.target.name;
        let value = event.target.value;

        formValues[name] = value;

        this.setState({formValues})
    }

    handleSubmit(event) {
        event.preventDefault();
        console.log(this.state.formValues);
    }

        render(){
        return (
        <center>
          <h2> Step 2: Time details</h2>
          <br/>
          <br/>
          <br/>
          <form onSubmit={this.handleSubmit.bind(this)}>
            <label>
              nya fält:
            </label>
            <br/>
            <input type="text" name="name" placeholder="Company Name" value={this.state.formValues["name"]} onChange={this.handleChange.bind(this)} />
            <br/>
            <br/>
            <label>
              cool beans:
            </label>
            <br/>
            <input type="text" name="email" placeholder="Job title" value={this.state.formValues["email"]} onChange={this.handleChange.bind(this)}/>
            <br/>
            <br/>
            <Link to='./step1'><input className="btn btn-info" type="submit" value="Back" /></Link>
            &nbsp;&nbsp;
            <Link to='./step3' ><input className="btn btn-info" type="submit" value="Next" /></Link>
          </form>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
        </center>
      )
    }
}

export default Test
