import React, { Component } from 'react';
import firebase from 'firebase';
import '../App.css';
import Calendar from 'react-calendar'

/* https://www.npmjs.com/package/react-firebase-storage-connector */

class Caldendar extends Component {

  constructor(){
    super();

    this.state = {
      url: ''
    }
  }


  componentDidMount(){
    firebase.auth().onAuthStateChanged(firebaseUser => {
      if(firebaseUser) {
        var uuid = firebaseUser.uid;
        var ref = firebase.database().ref("users/" + uuid);

        ref.on('value', snap => {
          var url = snap.val().profileImageUrl;
          this.setState({url: url});
        });
        }else {
          console.log("Not logged in")
        }
    });
  }


  render () {
    return(
      <div>
        <center><h2>Calendar</h2></center>
        <br/>
        <br/>
        <br/>
        <br/>
        <center><Calendar /></center>
      </div>
    )
  }

}

export default Caldendar;
