import React, { Component } from 'react';
import * as firebase from 'firebase';
import '../App.css';

class LoginTest extends Component {

  constructor(){
    super();
    this.handleClick = this.handleClick.bind(this);
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);

    this.state = {
      currentItem: '',
      username: '',
      items: [],
      user: null // <-- add this line
    }
  }

componentDidMount() {
  auth.onAuthStateChanged((user) => {
    if (user) {
      this.setState({ user });
    } 
  });


handleChange(e) {
  /* ... */
}


logout() {
  auth.signOut()
    .then(() => {
      this.setState({
        user: null
      });
    });
}

login() {
  auth.signInWithPopup(provider) 
    .then((result) => {
      const user = result.user;
      this.setState({
        user
      });
    });
}

render() {
  return (
    <div>
      <header>
        <div className="wrapper">
          <h1>Fun Food Friends</h1>
          {this.state.user ?
            <button onClick={this.logout}>Logout</button>                
            :
            <button onClick={this.login}>Log In</button>              
          }
        </div>
      </header>
      {this.state.user ?
        <div>
          <div className='user-profile'>
            <img src={this.state.user.photoURL} />
          </div>
        </div>
        :
        <div className='wrapper'>
          <p>You must be logged in to see the potluck list and submit to it.</p>
        </div>
      }
    </div>
  );
}


export default LoginTest;
