import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { bubble as Menu } from 'react-burger-menu'
//import { Dropdown, DropdownToggle, DropdownMenu } from 'react-bootstrap';
import '../App.css';
import { Icon } from 'react-icons-kit/';
import firebase from 'firebase';
import { menu, home3, envelop, search, switchIcon } from 'react-icons-kit/icomoon';


class SideNav extends Component {

  constructor(){
    super();

    this.state = {
      username: '',
      url: '',
      isLoggedIn: false
    }
  }


  componentDidMount(){
    firebase.auth().onAuthStateChanged(firebaseUser => {
      if(firebaseUser) {
        var uuid = firebaseUser.uid;
        var ref = firebase.database().ref("users/" + uuid);
        this.setState({isLoggedIn: true});
        ref.on('value', snap => {
          var name = snap.val().name;
          var url = snap.val().profileImageUrl;
          this.setState({url: url});
          this.setState({username: name});
        });
      }else {
        this.setState({isLoggedIn: false});
      }
    });
  }


  tick () {
    this.setState({count: (this.state.count + 1)})
  }


  render () {
    return(

      <Menu bodyClassName={"menuBody"} isOpen={true} noOverlay={true} width={'210px'} customBurgerIcon={<Icon size={64} icon={menu}/>}>

        {this.state.isLoggedIn &&
          <div style={{ color: 'white', paddingLeft: '32px', paddingBottom: '10px', paddingTop: '25px'}}>
            <img className='user-profile' src={this.state.url} alt="" width={100} height={100}/>
            <h4>{this.state.username}</h4>
          </div>
        }
        {!this.state.isLoggedIn &&
          <div>
            <br/><br/><br/><br/>
          </div>
        }
        <div style={{ color: 'white' }}>
          <Link to='./Home'><Icon size={32} icon={home3}/></Link>
          <Link to='./Messages'><Icon size={32} icon={envelop} className="menuIcons"/></Link>
          <Link to='./Search'><Icon size={32} icon={search}/></Link>
          <br/><br/>
        </div>

        <li><Link to='./CompanyProfile'>Company profile</Link></li>
        <li><Link to='./Applicants'>My job posts</Link></li>
        <li><Link to='./Favorites'>Interviewees</Link></li>
        <li><Link to='./Favorites'>Favorites</Link></li>
        <li><Link to='./Calendar'>Calendar</Link></li>
        <br/><br/>
        <li><Link className="createJobLink" to='./step1'>Create job post</Link></li>
        <br/>
        <hr/>
        <div>
          <h4 style={{ color: 'white'}} >Recent Messages</h4>
        </div>
        <div id='circle'>
          <h4 style={{ color: 'white', paddingLeft: '50px' }} >Emilia Nilsson</h4>
        </div>
        <div id='circle'>
          <h4 style={{ color: 'white', paddingLeft: '50px' }} >Josephine Ask</h4>
        </div>
        <div id='circle'>
          <h4 style={{ color: 'white', paddingLeft: '50px' }} >John Smith</h4>
        </div>
        <hr/>
        <div style={{ paddingRight: '32px', color: 'white', textAlign:"center" }}>
          <br/>
          {/*<Link to='./LoginLogout'><Icon size={32} icon={userCheck}/></Link>*/}
          <Link to='./LoginLogout'><Icon size={24} icon={switchIcon}/></Link>
        </div>



      </Menu>
    )
  }
}


export default SideNav;
