import React from 'react'
import '../App.css';
import { Link, formValues } from 'react-router-dom'

class Test extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            formValues: {},
            testvalue: props
        }


        console.log("props = " + this.state.testvalue);
    }

    handleChange(event) {
        event.preventDefault();
        let name = event.target.name;
        let value = event.target.value;
        //let formValues = this.state.formValues;
        formValues[name] = value;

        this.setState({formValues})
    }

    handleSubmit(event) {
        event.preventDefault();
        console.log(this.state.formValues);
    }

        render(){
        return (
        <center>
          <h2> 3: Record video questions  </h2>
          <br/>
          <br/>
          <form onSubmit={this.handleSubmit.bind(this)}>
            <label>
              nya fält:
            </label>
            <br/>
            <input type="text" name="name" placeholder="Company Name" value={this.state.formValues["name"]} onChange={this.handleChange.bind(this)} />
            <br/>
            <br/>
            <label>
              cool beans:
            </label>
            <br/>
            <input type="text" name="email" placeholder="Job title" value={this.state.formValues["email"]} onChange={this.handleChange.bind(this)}/>
            <br/>
            <br/>
            <Link to='./step2'><input className="btn btn-info" type="submit" value="Back" /></Link>
            &nbsp;&nbsp;
            <Link to='./step4'><input className="btn btn-info" type="submit" value="Next" /></Link>
          </form>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
        </center>
      )
    }
}

export default Test
