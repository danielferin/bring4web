import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import { BrowserRouter} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css';

/*
var config = {
    apiKey: "AIzaSyAQGuYIPm-UmJOsupIRDj5WNJET5Rs97Bk",
    authDomain: "fir-chat-eff7f.firebaseapp.com",
    databaseURL: "https://fir-chat-eff7f.firebaseio.com",
    projectId: "fir-chat-eff7f",
    storageBucket: "fir-chat-eff7f.appspot.com",
    messagingSenderId: "188194358628"
  };
firebase.initializeApp(config);
*/
ReactDOM.render((
  <BrowserRouter>
    <App/>
  </BrowserRouter>
), document.getElementById('root'))
