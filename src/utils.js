export function showUIElements(length){
  var bool = false;
  if(length > 0){bool = true}
  return bool;
}


export function checkForMatch(info, rulejson) {
	var rule = JSON.parse(rulejson);
  if(rule.match){
    return match(info,rule.match);
  }
  if(rule.and){
    return and(info,rule.and);
  }
  if(rule.not){
    return not(info,rule.not);
  }
  if(rule.or){
    return or(info,rule.or);
  }
}


function match(info,rule){
  var boolArray = [];
  for (var i = 0; i < rule.length; i++) {
    for (var j = 0; j < info.length; j++) {
      for (var l = 0; l < info[j].answer.length; l++) {
        if (findValue(rule[i].key, info[j].answer[l])) {
          for (var m = 1; m < info[j].answer.length; m++) {
            for (var n = 0; n < rule[i].value.length; n++) {
              if (checkop(info[j].answer[m], rule[i].op, rule[i].value[n])) {
                boolArray.push("true");
              } else {
              }
            }
          }
        }
      }
    }
  }
  return checkForTrueValue(boolArray);
}


function not(info,rule){
  var boolArray = [];
  for (var i = 0; i < rule.length; i++) {
    if(rule[i].match){
      if(match(info,rule[i].match)){
        boolArray.push("false")
      }else{
        boolArray.push("true")
      }
    }
  }
  return checkForTrueValue(boolArray);
}


function and(info,rule){
  var boolArray = [];
  for (var i = 0; i < rule.length; i++) {
    if(rule[i].match){
      if(match(info,rule[i].match)){
        boolArray.push("true")
      }else{
        boolArray.push("false")
      }
    }
    if(rule[i].not){
      if(not(info,rule[i].not)){
        boolArray.push("true")
      }else{
        boolArray.push("false")
      }
    }
    if(rule[i].and){
      if(and(info,rule[i].and)){
        boolArray.push("true")
      }else{
        boolArray.push("false")
      }
    }
    if(rule[i].or){
      if(or(info,rule[i].or)){
        boolArray.push("true")
      }else{
        boolArray.push("false")
      }
    }
  }
  return checkForTrueValue(boolArray);
}


function or(info,rule){
  var boolArray = [];
  for (var i = 0; i < rule.length; i++) {
    if(rule[i].match){
      boolArray.push(match(info,rule[i].match));
    }
    if(rule[i].not){
      boolArray.push(not(info,rule[i].not));
    }
    if(rule[i].and){
      boolArray.push(and(info,rule[i].and));
    }
    if(rule[i].or){
      boolArray.push(or(info,rule[i].or));
    }
  }

  if (boolArray.includes(true)) {
    return true;
  } else {
    return false;
  }
}


function checkForTrueValue(boolArray){
  var count = 0;
  if(boolArray.length === 0){
    return false;
  }
  for (var i = 0; i < boolArray.length; i++) {
    if(boolArray[i] === "true"){
      count = count + 1;
    }
  }
  return boolArray.length === count;
};


function findValue(v1, v2){
  return (v1 === v2);
};


function checkop(answer,op,rulevalue){
  var bool = false;
  switch(op) {
    case '==':
    if(answer === rulevalue){
      bool = true;
    }
    break;
    case '<':
    if(typeof(rulevalue) === "string"){
      rulevalue = parseInt(rulevalue,10);
    }
    if(answer < rulevalue){
      bool = true;
    }
    break;
    case '>':
    if(typeof(rulevalue) === "string"){
      rulevalue = parseInt(rulevalue,10);
    }
    if(answer > rulevalue){
      bool = true;
    }
    break;
    case 'not':
    if(answer !== rulevalue){
      bool = true;
    }
    break;
    case '<=':
    if(typeof(rulevalue) === "string"){
      rulevalue = parseInt(rulevalue,10);
    }
    console.log(typeof(rulevalue));
    if(answer <= rulevalue){
      bool = true;
    }
    break;
    default:
    bool = false;
  }
  return bool;
}


export function checkForPackages(matchingTreatments, key){
	var bool = false;
	for (var i = 0; i < matchingTreatments.length; i++) {
    if(matchingTreatments[i].package !== null){
      var temp = matchingTreatments[i].package.split(",");
      var matchingPackageName = temp[0].replace(/^\[/, "");
      if(matchingPackageName === key)(
        bool = true
      )
    }
	}
	return bool;
}
