import firebase from 'firebase'

var config = {
    apiKey: "AIzaSyAQGuYIPm-UmJOsupIRDj5WNJET5Rs97Bk",
    authDomain: "fir-chat-eff7f.firebaseapp.com",
    databaseURL: "https://fir-chat-eff7f.firebaseio.com",
    projectId: "fir-chat-eff7f",
    storageBucket: "fir-chat-eff7f.appspot.com",
    messagingSenderId: "188194358628"
  };
firebase.initializeApp(config);
export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export default firebase;

